from pathlib import Path


def delete_empty_folders(directory):
    """
    Delete all empty folders recursively in a directory tree.
    Args:
        directory: Path to the directory to clean up
    """
    if not directory:
        return

    path = Path(directory)
    if not path.is_dir():
        return

    # First recursively check all subdirectories
    for item in path.iterdir():
        if item.is_dir():
            delete_empty_folders(item)
    
    try:
        # Try to remove the directory if it's empty
        if path.exists() and not any(path.iterdir()):
            print(f"Deleting empty folder: {path}")
            path.rmdir()
    except Exception as e:
        print(f"Error deleting {path}: {e}")
