import os
import subprocess
import shutil
from contextlib import suppress
from send2trash import send2trash
from pathlib import Path
from image_helper import ImageInfo


def convert_heic(file_path):
	dest_path = file_path.with_suffix(".JPG")
	print("Converting: {}\n\t-> {}".format(file_path, dest_path))
	subprocess.check_call("magick convert {} {}".format(file_path, dest_path))
	if dest_path.exists():
		send2trash(str(file_path))


def convert_heic_in_dir(dir_path):
	for root, _, files in os.walk(dir_path):
		for file in files:
			file_path = Path(os.path.join(root, file))			
			if file_path.suffix.upper() in (".HEIC", ".HEIF"):
				convert_heic(file_path)


def clean_folders(dir_path, dir_pattern):
	all_dirs = set()
	for root, dirs, _ in os.walk(dir_path):
		for cur_dir in dirs:
			dir_path = Path(os.path.join(root, cur_dir))
			if cur_dir.upper() == dir_pattern:
				all_dirs.add(dir_path)
	for cur_dir in all_dirs: 
		print("Удаляем: {}".format(cur_dir))		
		with suppress(OSError):		
			shutil.rmtree(cur_dir)


def clean_images_with_effects(dir_path):
	files_to_delete = []
	for root, _, files in os.walk(dir_path):
		for file in files:
			file_path = Path(os.path.join(root, file))			
			if file.startswith("IMG_"):
				file_with_effects = file.replace("IMG_", "IMG_E")
				effects_path = Path(os.path.join(root, file_with_effects))
				if effects_path.exists() and ImageInfo(str(file_path)).date() == ImageInfo(str(effects_path)).date():
					files_to_delete.append(file_path)
	for file in files_to_delete:
		print("Отправляем в корзину файл, для которого есть копия с наложенными эффектам:\n\t {}".format(file))
		send2trash(file)


def rename_images_with_effects(dir_path):
	files_to_delete = []
	for root, _, files in os.walk(dir_path):
		for file in files:
			file_path = Path(os.path.join(root, file))			
			if file.startswith("IMG_E"):
				new_file = file.replace("IMG_E", "IMG_")
				new_file_path = Path(os.path.join(root, new_file))			
				print(f"Переименовываем:\n\t{file_path}\n\t->\n\t{new_file_path}")
				os.rename(file_path, new_file_path)


if __name__ == "__main__":
	#clean_folders(r"E:\\",  ".WDMC")
	convert_heic_in_dir(r"F:\Anna_iPhone")



