import shutil
import os


def copy_no_highres(source, dest):
    """Copy a directory structure overwriting existing files"""
    for root, dirs, files in os.walk(source):
        if root.endswith("highres"):
            continue
        for each_file in files:
            rel_path = root.replace(source, '').lstrip(os.sep)
            dest_path = os.path.join(dest, rel_path, each_file)
            rel_dir = os.path.join(dest, rel_path)
            source_path = os.path.join(root, each_file)
            if not os.path.isdir(rel_dir):
                print("Making {}".format(rel_dir))
                os.makedirs(rel_dir, exist_ok=True)
            if not os.path.isfile(dest_path):
                print("Copying to {}".format(dest_path))
                shutil.copy2(source_path, dest_path)
            elif os.stat(dest_path).st_size != os.stat(source_path).st_size:
                print("Overwriting {}".format(dest_path))
                shutil.copy2(source_path, dest_path)


if __name__ == "__main__":
    print("Начинаем работу.")
    copy_no_highres("P:\Roman_WindowsPhone", "D:\Photo\Roman_WindowsPhone")
    print("Завершаем работу.")