import filecmp
import os
import glob
import image_helper
from send2trash import send2trash

NAME_MESSAGE = """
NAME>-----------------------------------------
Найдены файлы с одинаковым именем:
1. {}
2. {}
"""

BINARY_MESSAGE = """
BINARY>-----------------------------------------
Найдены бинарно однаковые файлы:
1. {}
2. {}
"""



def get_date(image_path):
    info = image_helper.ImageInfo(image_path)    
    try:
        return info.date()
    except Exception:
        return None


class FileList:
    def __init__(self):
        self._files = set()

    def process_dir_recursivly(self, dir_path):
        for root, _, files in os.walk(dir_path):
            for file in files:
                self._files.add(os.path.join(root, file))

    def process_dir_by_glob(self, dir_wd):
        dirs = glob.iglob(dir_wd)
        for dir in dirs:
            self.process_dir_recursivly(dir)

    def files(self):
        return self._files


def compare_files_and_delete_if_duplicates(file1, file2, send_to_trash):
    if filecmp.cmp(file1, file2):
        print("Найдены дубликаты:\n\t 1) {} \n\t 2) {}".format(file1, file2))
        if send_to_trash:
            print("Отправляем в корзину:\n\t {}".format(file1))
            send2trash(file1)
        return True
    return False


def compare_files(list1, list2, send_to_trash):
    exclusive_files = list1.copy()
    possible_duplicates = set()

    for file1 in list1:
        if file1 not in exclusive_files:
            continue

        for file2 in list2:
            if any(x.endswith(y) for x in (file1, file2) for y in (".db", ".info")):
                continue

            if compare_files_and_delete_if_duplicates(file1, file2, send_to_trash):
                exclusive_files.remove(file1)
                break

            if os.path.basename(file1) == os.path.basename(file2):
                date = get_date(file1)
                if date is not None and date == get_date(file2):
                    print("Найдены файлы с одинаковыми именами и одинаковой датой:\n\t 1) {} \n\t 2) {}".format(file1, file2))                    
                    if send_to_trash:
                        print("Отправляем в корзину")
                        exclusive_files.remove(file1)
                        send2trash(file1)
                        break
                else:
                    print("Найдены файлы с одинаковыми именами, но не дубликаты:\n\t 1) {} \n\t 2) {}".format(file1, file2))
                    possible_duplicates.add(file1)

    possible_duplicates = [x for x in possible_duplicates if os.path.exists(x)]

    for file in possible_duplicates:
        exclusive_files.discard(file)


    return exclusive_files, possible_duplicates



def compare_dirs(dir1, dir2):
    diffs1 = dict()
    diffs2 = dict()
    same_files = list()
    for root1, dirs1, files1 in os.walk(dir1):
        for root2, dirs2, files2 in os.walk(dir2):
            for file1 in files1:
                for file2 in files2:
                    if any(x.endswith(y) for x in (file1, file2) for y in (".db", ".info")):
                        continue

                    real_path1 = os.path.join(root1, file1)
                    real_path2 = os.path.join(root2, file2)

                    if file1 not in same_files:
                        diffs1[file1] = real_path1

                    if file2 not in same_files:
                        diffs2[file2] = real_path2

                    if file1 == file2:
                        print(NAME_MESSAGE.format(real_path1, real_path2))
                        diffs1.pop(file1, None)
                        diffs2.pop(file2, None)
                        same_files.append(file1)

                    if filecmp.cmp(real_path1, real_path2):
                        print(BINARY_MESSAGE.format(real_path1, real_path2))


def print_list(files, print_dates):
    for file in sorted(files):
        print(file)     
        if print_dates:   
            print(">DATE> {}".format(get_date(file)))                


def process_duplicates(files_to_check_glob, files_etalon_glob, print_dates=True):
    print("Начинаем работу") 

    files_to_check = FileList()
    files_to_check.process_dir_by_glob(files_to_check_glob)

    files_etalon = FileList()
    files_etalon.process_dir_by_glob(files_etalon_glob)

    exlusive_files, possible_duplicates = compare_files(files_to_check.files(), files_etalon.files(), send_to_trash=True)

    print("------------------------------------------------------")
    print("Потенциальные дубли:")
    print_list(possible_duplicates, print_dates)    

    print("------------------------------------------------------")
    print("Эксклюзивы:")
    print_list(exlusive_files, print_dates)


if __name__ == "__main__":
    process_duplicates(
        files_to_check_glob=r"E:\Anna_iPhone_Sorted\2019*", 
        files_etalon_glob=r"E:\Фотографии\2019*")    