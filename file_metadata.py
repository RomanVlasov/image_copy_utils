from hachoir.parser import createParser
from hachoir.metadata import extractMetadata


def get_file_info(video_path):
    parser = createParser(video_path)
    with parser:
        metadata = extractMetadata(parser)
    return metadata


def get_file_creation_time(video_path):
    try:
        info = get_file_info(video_path)
        return info.get("creation_date")
    except Exception as e:
        print("Got error: {} for file '{}' when extracting creation date".format(e, video_path))
        return None


if __name__ == "__main__":
    info = get_file_info(r"P:\Фотографии\2017\2017_10_Португалия\iPhone_Рома\IMG_0575.MOV")
    print(info)