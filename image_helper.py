import PythonMagick
import os.path
from datetime import datetime, date

PHOTO_EXTENSIONS = set(['gif', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'wmf', 'cr2', 'heic'])
DATE_EXIF_ATTRIBUTES = ["EXIF:DateTimeOriginal","EXIF:DateTime"]


def get_image_tags(image_path):
    if not any(image_path.lower().endswith("." + x) for x in PHOTO_EXTENSIONS):
        return None

    tags = dict()

    try:
        image = PythonMagick.Image(image_path)
    except RuntimeError as e:
        print(e)
        return tags
    for tag in DATE_EXIF_ATTRIBUTES:
        tags[tag] = image.attribute(tag)
    return tags


class ImageInfo:
    def __init__(self, image_path):
        tags = get_image_tags(image_path)
        self._tags = dict() if tags is None else tags
        _, self._extension = os.path.splitext(image_path)

    def has_tags(self):
        return len(self._tags) > 0

    def extension(self):
        return self._extension

    def all_tags(self):
        return self._tags

    def date_original(self):
        for tag in DATE_EXIF_ATTRIBUTES:            
            date = self._tags.get(tag)
            if date is not None:
                return date
        return None

    def parse_date(self):
        return datetime.strptime(str(self.date_original()), "%Y:%m:%d %H:%M:%S")

    def date(self):
        try:
            return self.parse_date().date()
        except Exception:
            return None

    def camera_name(self):
        result = self._tags.get("Image Model")
        if str(result).startswith("XZ-1"):
            result = "XZ-1_Olymp"
        return result

    def print_all_tags(self):
        for key, value in self._tags.items():
            print("'{}': '{}'".format(key, value))


if __name__ == "__main__":
    root = r"E:\Roman_iPhone"
    #dirs = glob.iglob(os.path.join(root, "2008*"))
    dirs = [root]
    for dir in dirs:
        for root, _, files in os.walk(dir):
            for file in files:
                file_path = os.path.join(root, file)
                print("--------------------------------")
                print(file_path)
                info = ImageInfo(file_path)
                if info.has_tags():
                    print(info.date_original())
                    print(info.parse_date())


