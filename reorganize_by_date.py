import image_helper
import file_metadata
import os
import shutil
import datetime
from functools import partial
from send2trash import send2trash
from detect_duplicates import compare_files_and_delete_if_duplicates
from pathlib import Path
from calendar import monthrange


def get_day(creation_date):
    return creation_date.date()


def get_day_time(creation_date):
    return creation_date.time()


def debug_rename(file_path, new_name):
    pass


def os_rename(file_path, new_name):
    os.rename(file_path, new_name)


def get_new_file_path(file_path, new_name, extension):
    dir_path = os.path.dirname(file_path)
    new_path = Path(dir_path) / (new_name + extension.lower())
    i = 2
    while new_path.exists():
        new_path = Path(dir_path) / f"{new_name}_{i}{extension.lower()}"
        i += 1
    return new_path


def process_exif_image(file_path):
    info = image_helper.ImageInfo(file_path)
    if info.has_tags():
        creation_date = info.parse_date()
        print(f"{file_path}")
        print(f"{creation_date}")
        camera_name = str(info.camera_name()).replace(" ", "_")
        return creation_date, camera_name
    raise RuntimeError("Not an image")


def process_metadata(file_path, rename_time_dict, delta_hours=3):
    if image_helper.ImageInfo(file_path).has_tags():
         raise RuntimeError("Already processed")
    name, extension = os.path.splitext(file_path)
    if name in rename_time_dict:
        creation_date = rename_time_dict[name]
    else:
        creation_date = file_metadata.get_file_creation_time(file_path) + datetime.timedelta(hours=delta_hours)
    camera_name = "Meta"
    return creation_date, camera_name

def rename_by_strategy(dir_path, get_date_func, info_getter, rename_strategy):
    rename_dict = dict()
    for root, _, files in os.walk(dir_path):
        for file in files:
            file_path = os.path.join(root, file)

            try:
                creation_date, camera_name = info_getter(file_path)
            except RuntimeError:
                continue
            except ValueError:
                continue

            name, extension = os.path.splitext(file_path)
            print("{}".format(file_path))
            time_value = str(get_date_func(creation_date)).replace(':', '-')
            new_name = "img_{}_{}".format(time_value, camera_name)
            rename_dict[name] = creation_date        
            new_file_path = get_new_file_path(file_path, new_name, extension)
            print("\t-> {}".format(new_file_path.name))
            rename_strategy(file_path, new_file_path)


    return rename_dict


def rename_by_date(dir_path, get_date_func, rename_strategy, gmt_delta=3):
    rename_time_dict = rename_by_strategy(dir_path, get_date_func, process_exif_image, rename_strategy)
    metadata_strategy = partial(process_metadata, rename_time_dict=rename_time_dict, delta_hours=gmt_delta)
    rename_by_strategy(dir_path, get_date_func, metadata_strategy, rename_strategy)


def debug_file_mover(file_path, dest_dir):
    pass


def shutil_file_mover(file_path, dest_dir):
    os.makedirs(dest_dir, exist_ok=True)
    try:
        shutil.move(file_path, dest_dir)
    except Exception as e:
        dest_path = os.path.join(dest_dir, os.path.basename(file_path))
        if not compare_files_and_delete_if_duplicates(file_path, dest_path, True):
            raise e


class DirectoryByDayProvider:
    def __init__(self, move_dict):
        self._move_dict = move_dict

    def get_directory(self, img_date):
        if img_date in self._move_dict:
            return self._move_dict[img_date]
        return None


class DirectoryByMonthProvider:
    def __init__(self, move_dict):
        self._move_dict = move_dict

    def get_directory(self, img_date):
        if img_date is None:
            return None
        for key in self._move_dict.keys():
            if img_date.year == key[0] and img_date.month == key[1]:
                return self._move_dict[key]
        return None


class PhotoOrganizer:
    def __init__(self, mover):
        self._mover = mover

    def move_by_date(self, source_date, file_path, dir_provider):
        dest_dir = dir_provider.get_directory(source_date)
        if dest_dir is not None:
            print("{} \n\t-> {}".format(file_path, dest_dir))
            self._mover(file_path, dest_dir)
        else:
            print("Wrong date: {}, {}".format(file_path, source_date))

    def reorganize(self, dir_path, dir_provider):        
        for root, _, files in os.walk(dir_path):
            for file in files:
                file_path = os.path.join(root, file)
                if file_path.lower().endswith(".aae"):                                               
                    print("Отправляем в корзину файл с дополнительными модификациями: {}".format(file_path))
                    send2trash(file_path)
                    continue

                info = image_helper.ImageInfo(file_path)
                if info.has_tags():
                    print("Checking image: {}".format(file_path))
                    self.move_by_date(info.date(), file_path, dir_provider)
                else:
                    video_date = file_metadata.get_file_creation_time(file_path)
                    if video_date is not None:
                        print("Checking video: {}".format(file_path))
                        self.move_by_date(video_date.date(), file_path, dir_provider)                    
                    else:
                        print("Skipping: {}".format(file_path))        


def reorganize_by_day(from_day, month, year, source_dir, dest_dir, start_index=0):
    move_dict = {}
    i = start_index
    for day in range(from_day, monthrange(year, month)[1] + 1):
        move_dict[datetime.date(year, month, day)] = os.path.join(dest_dir, "День_{}".format(i))
        i += 1

    photo_organizer = PhotoOrganizer(shutil_file_mover)
    photo_organizer.reorganize(source_dir, DirectoryByDayProvider(move_dict))


def create_move_by_month_dict(root, postfix):
    move_dict = dict()
    for year in range(2009, 2030):
        for month in range(1, 13):
            key = (year, month)
            move_dict[key] = os.path.join(root, "{}_{m:02d}_{p}".format(year, m=month, p=postfix))
    return move_dict


def reorganize_by_month():
    move_dict = create_move_by_month_dict(r"E:\Roman_iPhone_Sorted", "Roman_iPhone")
    photo_organizer = PhotoOrganizer(shutil_file_mover)
    photo_organizer.reorganize(r"E:\Roman_iPhone", DirectoryByMonthProvider(move_dict))


def rename_dir_by_date():
    rename_by_date(r"D:\Photo", get_day_time, os_rename)


if __name__ == "__main__":
    #reorganize_by_month()
    #reorganize_by_day(from_day=30, month=10, year=2023, source_dir=r"F:\Roman_iPhone_Sorted\Гонконг", dest_dir=r"F:\Roman_iPhone_Sorted\Гонконг_2")
    rename_by_date(r"F:\Roman_iPhone_Sorted\2023_10-11_Гонконг", get_day_time, os_rename, gmt_delta=8)
    #rename_dir_by_date()


