import os
import os.path
import shutil
from PIL import Image


HIGHRES_DIR = "highres"


def detect_highres_images(dir_path):
    result = list()
    for root, dirs, files in os.walk(dir_path):
        if HIGHRES_DIR in dirs:
            continue
        if root.endswith(HIGHRES_DIR):
            continue
        for file in files:
            if file.endswith("_highres.jpg"):
                result.append((root, file))
            elif file.endswith("jpg"):
                image_path = os.path.join(root, file)
                with Image.open(image_path) as image:
                    if (any(x >= 5376 for x in image.size)):
                        result.append((root, file))
    return result


def convert_size(original_size):
    result = list(original_size)
    for i in range(0, len(original_size)):
        if original_size[i] == 5376:
            result[i] = 3072
        elif original_size[i] == 3024:
            result[i] = 1728
    return result



def reduce_images(files_detected):
    for root, file in files_detected:
        source_file = os.path.join(root, file)
        dest_file = os.path.join(root, file.replace("__highres", "").replace("_highres", ""))
        if not os.path.exists(dest_file):
            im = Image.open(source_file)
            new_size = convert_size(im.size)
            im.thumbnail(new_size, Image.ANTIALIAS)
            print("Forming new image: {}\nWith size:{}".format(dest_file, new_size))
            im.save(dest_file, "JPEG")


def verify_all_present(dir_path):
    for root, _, files in os.walk(dir_path):
        if root.endswith(HIGHRES_DIR):
            for file in files:
                file_name = os.path.basename(file).replace("__highres", "").replace("_highres", "")
                dir_path = os.path.join(root, "..")
                check_path = os.path.join(dir_path, file_name)
                if (not os.path.exists(check_path)) and (file_name.find("_2016") != -1):
                    print("Not exists: {}".format(check_path))
                    shutil.copy2(os.path.join(root, file), check_path)


def move_to_highres_folder(files):
    for dir, file in files:
        highres_dir = os.path.join(dir, HIGHRES_DIR)
        if not os.path.isdir(highres_dir):
            os.makedirs(highres_dir)
        original_file = os.path.join(dir, file)
        dest_file = os.path.join(highres_dir, file)
        if not os.path.exists(dest_file):
            print("Copying to: {}".format(dest_file))
            shutil.copy2(original_file, dest_file)
        print("Removing: {}".format(original_file))
        os.remove(original_file)


def rename_highres_files(files):
    for dir, file in files:
        file_path = os.path.join(dir, file)
        if file_path[:-4].endswith("_highres"):
            continue
        new_file_name =  file_path[:-4] + "_highres" + file_path[-4:]
        print("Renaming {} to {}".format(file_path, new_file_name))
        os.rename(file_path, new_file_name)


if __name__ == "__main__":
    #verify_all_present("P:\Roman_WindowsPhone")

    files_detected = detect_highres_images("P:\Roman_WindowsPhone")
    print(files_detected)
    rename_highres_files(files_detected)

    files_detected = detect_highres_images("P:\Roman_WindowsPhone")
    reduce_images(files_detected)
    move_to_highres_folder(files_detected)
