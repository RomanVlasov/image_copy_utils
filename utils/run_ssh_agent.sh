#!/bin/bash
# Starting ssh-agent
eval `ssh-agent -s`
# Loading ssh key for direct connection with bitbucket
ssh-add ~/.ssh/id_rsa
# Set interactive mode because we need to capture ssh-key password
bash -i